package main

import (
	"bytes"
	"fmt"
	"html/template"
	"log"
	"net/http"
	"os"

	"github.com/bmizerany/pat"
	"github.com/joho/godotenv"
)

func main() {
	mux := pat.New()
	mux.Get("/", http.HandlerFunc(index))
	mux.Post("/", http.HandlerFunc(send))
	mux.Get("/confirmation", http.HandlerFunc(confirmation))

	log.Println("Listening...")
	http.ListenAndServe(":3000", mux)
}

func index(w http.ResponseWriter, r *http.Request) {
	render(w, "templates/index.html", nil)
}

// init is invoked before main()
func init() {
	// loads values from .env into the system
	if err := godotenv.Load(); err != nil {
		log.Print("No .env file found")
	}
}

func send(w http.ResponseWriter, r *http.Request) {
	var api_token = os.Getenv("api_token")

	msg := &Message{
		Email:       r.FormValue("email"),
		Summary:     r.FormValue("summary"),
		Description: r.FormValue("description"),
		StoryType:   r.FormValue("storytype"),
		Deadline:    r.FormValue("deadline"),
	}

	if msg.Validate() == false {
		render(w, "templates/index.html", msg)
		return
	}

	// Make API call to Clubhouse.
	url := "https://api.clubhouse.io/api/v2/stories?token=" + api_token

	var jsonStr = []byte(`{"description": "` + msg.Description + ` - Requested by ` + msg.Email + `", "deadline": "` + msg.Deadline + `" , "epic_id": ` + os.Getenv("epic_id") + `, "name": "` + msg.Summary + `", "project_id": ` + os.Getenv("project_id") + `, "story_type": "` + msg.StoryType + `"}`)
	req, err := http.NewRequest("POST", url, bytes.NewBuffer(jsonStr))
	req.Header.Set("Content-Type", "application/json")
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		fmt.Printf("The HTTP request failed with error %s\n", err)
		return
		//panic(err)
	}
	defer resp.Body.Close()

	// Send message in an email
	//if err := msg.Deliver(); err != nil {
	//	http.Error(w, err.Error(), http.StatusInternalServerError)
	//	return
	//}

	// Redirect to confirmation page
	http.Redirect(w, r, "/confirmation", http.StatusSeeOther)
}

func confirmation(w http.ResponseWriter, r *http.Request) {
	render(w, "templates/confirmation.html", nil)
}

func render(w http.ResponseWriter, filename string, data interface{}) {
	tmpl, err := template.ParseFiles(filename)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
	if err := tmpl.Execute(w, data); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}
