package main

import (
	"fmt"
	"net/smtp"
	"regexp"
	"strings"
)

type Message struct {
	Email       string
	Summary     string
	Description string
	StoryType   string
	Deadline    string
	Errors      map[string]string
}

func (msg *Message) Deliver() error {
	to := []string{"someone@example.com"}
	body := fmt.Sprintf("Reply-To: %v\r\nSubject: New Message\r\n%v", msg.Email, msg.Summary+" "+msg.Description+" "+msg.StoryType+" "+msg.Deadline)

	username := "you@gmail.com"
	password := "..."
	auth := smtp.PlainAuth("", username, password, "smtp.gmail.com")

	return smtp.SendMail("smtp.gmail.com:587", auth, msg.Email, to, []byte(body))
}

func (msg *Message) Validate() bool {
	msg.Errors = make(map[string]string)

	re := regexp.MustCompile(".+@.+\\..+")
	matched := re.Match([]byte(msg.Email))
	if matched == false {
		msg.Errors["Email"] = "Please enter a valid email address"
	}

	if strings.TrimSpace(msg.Summary) == "" {
		msg.Errors["Summary"] = "Please provide a summary"
	}

	if strings.TrimSpace(msg.Description) == "" {
		msg.Errors["Description"] = "Please provide a description"
	}

	if strings.TrimSpace(msg.StoryType) == "" {
		msg.Errors["StoryType"] = "Please provide a type of request"
	}

	if strings.TrimSpace(msg.Deadline) == "" {
		msg.Errors["Deadline"] = "Please provide a due date"
	}

	return len(msg.Errors) == 0
}
